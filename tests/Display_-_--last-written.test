#!/bin/sh
#
# Check that "--last-written" works.

# Allow all tests to be skipped, e.g. during a release build
test "${SKIP_ALL_TESTS}" = "1" && exit 77

true "${testSubject:?not set - call this from 'make check'}"
true "${workFile1:?not set - call this from 'make check'}"

# Slowly write a sequence of numbers (rate-limited by another `pv' instance)
# and watch the "--last-written" output of a second `pv' instance to make
# sure the buffer contents are visible (we use "-B" to disable splice mode).
seq -w 3 1 100 \
| "${testSubject}" -qL 200 \
| "${testSubject}" -f -B 1024 -A 16 -i 0.2 >/dev/null 2>"${workFile1}"

differentLines=$(tr '\r' '\n' < "${workFile1}" | sed '/^ *$/d' | sort | uniq | wc -l | tr -dc '0-9')
lastLine=$(tr '\r' '\n' < "${workFile1}" | sed '/^ *$/d' | sed -n '$p')

# Even on very slow hosts, we should see at least 3 different output lines -
# nothing at the start, something in the middle, and the last few numbers at
# the end.
if ! test "${differentLines}" -gt 2; then
	echo "fewer than 3 different outputs seen"
	tr '\r' '\n' < "${workFile1}" | sed '/^ *$/d' | sort | uniq
	exit 1
fi

expectedLastLine="097.098.099.100."
if ! test "${lastLine}" = "${expectedLastLine}"; then
	echo "final output differs from expected value"
	echo "expected value: [${expectedLastLine}]"
	echo "observed value: [${lastLine}]"
	exit 1
fi

exit 0
