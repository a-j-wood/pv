#!/bin/sh
#
# Run valgrind's memory checker against a process using the --watchfd
# option.

# Allow all tests to be skipped, e.g. during a release build
test "${SKIP_ALL_TESTS}" = "1" && exit 77

true "${sourcePath:?not set - call this from 'make check'}"
true "${testSubject:?not set - call this from 'make check'}"
true "${workFile1:?not set - call this from 'make check'}"
true "${workFile2:?not set - call this from 'make check'}"
true "${workFile3:?not set - call this from 'make check'}"
true "${workFile4:?not set - call this from 'make check'}"

# Load the valgrind function.
. "${sourcePath}/tests/run-valgrind.sh"

# Skip the test if "-d" is not available.
if ! "${testSubject}" -h | grep -Fq ' -d'; then
	echo "no \`--watchfd' / \`-d' option on this platform"
	exit 77
fi

# Skip the test if "-d" does not work.
sleep 2 &
pid=$!
sleep 0.1
"${testSubject}" -d "${pid}" -f -i 0.5 >/dev/null 2>"${workFile1}"
if grep -Fq ' -d: not available' "${workFile1}"; then
	echo "no \`--watchfd' / \`-d' option on this platform"
	exit 77
fi

# Check "--watchfd PID:FD".
# See the "Watchfd" tests for more detail.
seq 1 100 > "${workFile1}"
true > "${workFile3}"
# shellcheck disable=SC2030
(while test -e "${workFile3}" && ! test -s "${workFile3}"; do sleep 0.1; done; sleep 1; read -r line; sleep 1; read -r line; sleep 1) <"${workFile1}" &
pid=$!
sleep 0.1
{ runWithValgrind -P "${workFile3}" -d "${pid}:0" -f -i 0.5 >/dev/null 2>&1; } 4>&1 || exit 1

# Check "--watchfd PID".
seq 1 100 > "${workFile1}"
seq 1 300 > "${workFile2}"
true > "${workFile3}"
# shellcheck disable=SC2030
(
while test -e "${workFile3}" && ! test -s "${workFile3}"; do sleep 0.1; done
sleep 1
read -r line
exec 9<"${workFile2}"
sleep 1
exec 8<"${workFile1}"
exec 7</dev/null
sleep 1
exec 9<&-
# shellcheck disable=SC2034
read -r line
sleep 1
) <"${workFile1}" &
pid=$!
sleep 0.1
{ runWithValgrind -P "${workFile3}" -d "${pid}" -f -i 0.5 >/dev/null 2>&1; } 4>&1 || exit 1

exit 0
