#!/bin/sh
#
# Check that numeric output gives a timer when used with -t.

# Allow all tests to be skipped, e.g. during a release build
test "${SKIP_ALL_TESTS}" = "1" && exit 77

true "${testSubject:?not set - call this from 'make check'}"
true "${workFile1:?not set - call this from 'make check'}"

# Process 100 bytes at 100 bytes per second, updating every 0.1 seconds for
# around 10 output lines.
#
dd if=/dev/zero bs=100 count=1 2>/dev/null \
| "${testSubject}" -s 100 -n -t -i 0.1 -L 100 >/dev/null 2>"${workFile1}"

lineCount=$(wc -l < "${workFile1}" | tr -dc '0-9')
timesCount=$(tr ',' '.' < "${workFile1}" | awk '{print int(10*$1)}' | sort -n | uniq | wc -l | tr -dc '0-9')
finalPercentage=$(sed -n '$p' < "${workFile1}" | awk '{print $2}')

# The number of output lines should be >7 and <13, and the number of
# different elapsed times should be at least 6. The last percentage should
# be 100.
#
if ! test "${lineCount}" -gt 7; then
	echo "fewer than 8 output lines (${lineCount})"
	exit 1
fi
if ! test "${lineCount}" -lt 13; then
	echo "more than 12 output lines (${lineCount})"
	exit 1
fi
if ! test "${timesCount}" -gt 5; then
	echo "fewer than 6 different elapsed times (${timesCount})"
	exit 1
fi
if ! test "${finalPercentage}" = "100"; then
	echo "final percentage was not 100 (${finalPercentage})"
	exit 1
fi

exit 0
