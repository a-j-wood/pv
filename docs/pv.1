.TH PV 1 2025-01-28 pv-1.9.31 "User Commands"
.\"
.SH NAME
pv \- monitor the progress of data through a pipe
.\"
.SH SYNOPSIS
.B pv
[\fIOPTION\fR]... [\fIFILE\fR]...
.PP
\fBpv\fR \fB\-d\fR|\fB\-\-watchfd\fR \fIPID\fR[:\fIFD\fR] [\fIOPTION\fR]...
.PP
\fBpv\fR \fB\-R\fR|\fB\-\-remote\fR \fIPID\fR [\fIOPTION\fR]...
.\"
.SH DESCRIPTION
Show the progress of data through a pipeline by giving information such as
time elapsed, percentage completed (with progress bar), current throughput
rate, total data transferred, and ETA.
.PP
Each \fIFILE\fR is copied to standard output.
With no \fIFILE\fR, or when \fIFILE\fR is \*(lq-\*(rq, standard input is
read.
This is the same behaviour as \fBcat\fR(1).
.\"
.SH OPTIONS
.\"
.SS "Display switches"
If no display switches are specified, \fBpv\fR behaves as if
\*(lq\fB\-\-progress\fR\*(rq, \*(lq\fB\-\-timer\fR\*(rq,
\*(lq\fB\-\-eta\fR\*(rq, \*(lq\fB\-\-rate\fR\*(rq, and
\*(lq\fB\-\-bytes\fR\*(rq had been given.
Otherwise, only those display types that are explicitly switched on will be
shown.
.TP
.B \-p, \-\-progress
Turn the progress bar on.
If any inputs are not files, or are unreadable, and no size was explicitly
given with \*(lq\fB\-\-size\fR\*(rq, the progress bar cannot indicate how
close to completion the transfer is, so it will just move left and right to
indicate that data is moving - or, with \*(lq\fB\-\-gauge\fR\*(rq, the bar
will indicate the current rate as a percentage of the maximum rate seen so
far.
.TP
.B \-t, \-\-timer
Turn the timer on.
This will display the total elapsed time that \fBpv\fR has been running for.
.TP
.B \-e, \-\-eta
Turn the ETA countdown on.
This will estimate, based on current transfer rates and the total data size,
how long it will be before completion.
The countdown is prefixed with \*(lqETA\*(rq.
This option will have no effect if the total data size cannot be determined.
.TP
.B \-I, \-\-fineta
Turn the ETA countdown on, but display the estimated local time at which the
transfer will finish, instead of the amount of time remaining.
When the estimated time is more than 6 hours in the future, the date is
shown as well.
The time is prefixed with \*(lqFIN\*(rq for finish time.
As with \*(lq\fB\-\-eta\fR\*(rq, this option will have no effect if the
total data size cannot be determined.
.TP
.B \-r, \-\-rate
Turn the rate counter on.
This will display the current rate of data transfer.
The rate is shown in square brackets \*(lq[]\*(rq.
.TP
.B \-a, \-\-average\-rate
Turn the average rate counter on.
This will display the current average rate of data transfer, over the last
30 seconds by default (see \*(lq\fB\-\-average-rate-window\fR\*(rq).
The average rate is shown in brackets \*(lq()\*(rq.
.TP
.B \-b, \-\-bytes
Turn the total byte counter on.
This will display the total amount of data transferred so far.
.TP
.B \-T, \-\-buffer\-percent
Turn on the transfer buffer percentage display.
This will show the percentage of the transfer buffer in use.
Implies \*(lq\fB\-\-no\-splice\fR\*(rq.
The transfer buffer percentage is shown in curly brackets \*(lq{}\*(rq.
.TP
.BI \-A\  NUM \fR,\ \fB\-\-last\-written\  NUM
Show the last \fINUM\fR bytes written.
Implies \*(lq\fB\-\-no\-splice\fR\*(rq.
.TP
.BI \-F\  FORMAT \fR,\ \fB\-\-format\  FORMAT
Ignore all of the above options and instead use the format string
\fIFORMAT\fR to determine the output format.
See the \fBFORMATTING\fR section below.
.TP
.B \-n, \-\-numeric
Numeric output.
Instead of giving a visual indication of progress, write an integer
percentage, one per line, on standard error, suitable for passing to a tool
such as \fBdialog\fR(1).
Note that \*(lq\fB\-\-force\fR\*(rq is not required if
\*(lq\fB\-\-numeric\fR\*(rq is being used.
.IP
Combining \*(lq\fB\-\-numeric\fR\*(rq with \*(lq\fB\-\-bytes\fR\*(rq will
cause the number of bytes processed so far to be output instead of a
percentage.
Adding \*(lq\fB\-\-line\-mode\fR\*(rq as well as \*(lq\fB\-\-bytes\fR\*(rq
writes the number of lines instead of bytes or a percentage.
Adding \*(lq\fB\-\-rate\fR\*(rq adds the transfer rate to each output line
(if \*(lq\fB\-\-bytes\fR\*(rq is also in use, the rate comes after the
byte/line count).
Adding \*(lq\fB\-\-timer\fR\*(rq prefixes each output line with the elapsed
time so far, as a decimal number of seconds.
.IP
Combining \*(lq\fB\-\-numeric\fR\*(rq with \*(lq\fB\-\-format\fR\*(rq allows
for custom output.  The default format string components for
\*(lq\fB\-\-numeric\fR\*(rq are
\*(lq\fB%t\~%b\~%r\~%{progress\-amount\-only}\fR\*(rq in that order, each
item being active or inactive according to the rules above (so the default
with no other options is \*(lq\fB%{progress\-amount\-only}\fR\*(rq.
.TP
.B \-q, \-\-quiet
No output.
Useful if the \*(lq\fB\-\-rate\-limit\fR\*(rq option is being used on its
own to limit the transfer rate of a pipe.
.\"
.SS "Output modifiers"
.TP
.B \-8, \-\-bits
Use bits instead of bytes for the byte and rate counters.
The output suffix will be \*(lqb\*(rq instead of \*(lqB\*(rq.
.TP
.B \-k, \-\-si
Display and interpret suffixes as multiples of 1000 rather than the default
of 1024.
Note that this only takes effect on options after this one, so for
consistency, specify this option first.
.TP
.B \-W, \-\-wait
Wait until the first byte has been transferred before showing any progress
information or calculating any ETAs.
Useful if the program you are piping to or from requires extra information
before it starts, such as when piping data into \fBgpg\fR(1) or
\fBmcrypt\fR(1) which require a passphrase before data can be processed.
.TP
.BI \-D\  SEC \fR,\ \fB\-\-delay\-start\  SEC
Wait until \fISEC\fR seconds have passed before showing any progress
information, for example in a script where you only want to show a progress
bar if it starts taking a long time.
The value of \fISEC\fR can be a decimal such as \*(lq0.5\*(rq.
.TP
.BI \-s\  SIZE \fR,\ \fB\-\-size\  SIZE
Assume the total amount of data to be transferred is \fISIZE\fR bytes when
calculating percentages and ETAs.
A suffix of \*(lqK\*(rq, \*(lqM\*(rq, \*(lqG\*(rq, or \*(lqT\*(rq can be
added to denote kibibytes (*1024), mebibytes, gibibytes, tebibytes.
If \*(lq\fB\-\-si\fR\*(rq appears before this option, suffixes will denote
kilobytes (*1000), megabytes, and so on instead.
.IP
If \fISIZE\fR starts with \*(lq\fB@\fR\*(rq, the size of file whose name
follows the @ will be used.
.TP
.B \-g, \-\-gauge
If the progress bar is shown but the size is not known, then instead of
moving the bar left and right to show progress, show the current transfer
rate as a percentage of the maximum rate seen so far.
.TP
.B \-l, \-\-line\-mode
Instead of counting bytes, count lines (newline characters).
The progress bar will only move when a new line is found, and the value
passed to \*(lq\fB\-\-size\fR\*(rq will be interpreted as a line count.
.IP
If this option is used without \*(lq\fB\-\-size\fR\*(rq, the "total size"
(in this case, total line count) is calculated by reading through all input
files once before transfer starts.
If any inputs are pipes or non-regular files, or are unreadable, the total
size will not be calculated.
.TP
.B \-0, \-\-null
Count lines as terminated with a null byte instead of with a newline.
This option implies \*(lq\fB\-\-line\-mode\fR\*(rq.
.TP
.BI \-i\  SEC \fR,\ \fB\-\-interval\  SEC
Wait \fISEC\fR seconds between updates.
The default is to update every second.
The value of \fISEC\fR can be a decimal such as \*(lq0.1\*(rq.
.TP
.BI \-m\  SEC \fR,\ \fB\-\-average-rate-window\  SEC
Compute current average rate over a \fISEC\fR seconds window for average
rate and ETA calculations.
The default is 30 seconds.
The value must be an integer.
.TP
.BI \-w\  WIDTH \fR,\ \fB\-\-width\  WIDTH
Assume the terminal is \fIWIDTH\fR columns wide, instead of trying to work
it out (or assuming 80 if it cannot be guessed).
If this option is used, the output width will not be adjusted if the width
of the terminal changes while the transfer is running.
.TP
.BI \-H\  HEIGHT \fR,\ \fB\-\-height\  HEIGHT
Assume the terminal is \fIHEIGHT\fR rows high, instead of trying to work it
out (or assuming 25 if it cannot be guessed).
If this option is used, the output height will not be adjusted if the height
of the terminal changes while the transfer is running.
.TP
.BI \-N\  NAME \fR,\ \fB\-\-name\  NAME
Prefix the output information with \fINAME\fR.
Useful in conjunction with \*(lq\fB\-\-cursor\fR\*(rq if you have a
complicated pipeline and you want to be able to tell different parts of it
apart.
.TP
.BI \-u\  STYLE \fR,\ \fB\-\-bar\-style\  STYLE
Change the default progress bar style shown by \*(lq\fB\-\-progress\fR\*(rq,
or by the \*(lq\fB\-\-format\fR\*(rq sequences \*(lq\fB%{progress}\fR\*(rq
or \*(lq\fB%{progress\-bar\-only}\fR\*(rq, to \fISTYLE\fR.
The \fISTYLE\fR can be one of \fBplain\fR (the default), \fBblock\fR,
\fBgranular\fR, or \fBshaded\fR.
These styles are described in the \fBFORMATTING\fR section below.
.TP
.BI \-x\  SPEC \fR,\ \fB\-\-extra\-display\  SPEC
As well as displaying progress to the terminal, also write it to \fISPEC\fR.
The \fISPEC\fR must start with a comma-separated list of destinations, and
can optionally be followed by a colon and a format string.
The destinations can be \fBwindowtitle\fR or \fBwindow\fR for the xterm
window title, and \fBprocesstitle\fR, \fBproctitle\fR, \fBprocess\fR, or
\fBproc\fR for the process title displayed by \fBps\fR(1).
If a format string is not supplied, the same format is used as for the
terminal.
For example, \*(lq\fB\-x\~'window,process:%t\~%b\~%r'\fR\*(rq will show the
elapsed time, bytes transferred, and rate, in both the window title and the
process title.
.TP
.B \-v, \-\-stats
At the end of the transfer, write an additional line showing the transfer
rate minimum, maximum, mean, and standard deviation.
The values are always in bytes per second (or bits, with
\*(lq\fB\-\-bits\fR\*(rq).
.TP
.B \-f, \-\-force
Force output.
Normally, \fBpv\fR will not output any visual display if standard error is
not a terminal.
This option forces it to do so.
.TP
.B \-c, \-\-cursor
Use cursor positioning escape sequences instead of just using carriage
returns.
This is useful in conjunction with \*(lq\fB\-\-name\fR\*(rq if you are using
multiple \fBpv\fR invocations in a single pipeline.
.\"
.SS "Data transfer modifiers"
.TP
.BI \-o\  FILE \fR,\ \fB\-\-output\  FILE
Write data to \fIFILE\fR instead of standard output.
If the file already exists, it will be truncated.
.TP
.BI \-L\  RATE \fR,\ \fB\-\-rate-limit\  RATE
Limit the transfer to a maximum of \fIRATE\fR bytes per second.
The same suffixes as \*(lq\fB\-\-size\fR\*(rq can be used.
.TP
.BI \-B\  BYTES \fR,\ \fB\-\-buffer-size\  BYTES
Use a transfer buffer size of \fIBYTES\fR bytes.
The same suffixes as \*(lq\fB\-\-size\fR\*(rq can be used.
The default buffer size is the block size of the input file's filesystem
multiplied by 32 (512KiB max), or 400KiB if the block size cannot be
determined.
This can be useful on platforms like macOS with pipelines that perform
better with specific buffer sizes such as 1024.
Implies \*(lq\fB\-\-no\-splice\fR\*(rq.
.TP
.B \-C, \-\-no-splice
Never use \fBsplice\fR(2), even if it would normally be possible.
The \fBsplice\fR(2) system call is a more efficient way of transferring data
from or to a pipe than regular \fBread\fR(2) and \fBwrite\fR(2), but means
that the transfer buffer may not be used.
This prevents \*(lq\fB\-\-buffer\-percent\fR\*(rq and
\*(lq\fB\-\-last\-written\fR\*(rq from working, cannot work with
\*(lq\fB\-\-discard\fR\*(rq, and makes \*(lq\fB\-\-buffer\-size\fR\*(rq
redundant, so using any of those options automatically switches on
\*(lq\fB\-\-no\-splice\fR\*(rq.
Switching on this option results in a small loss of transfer efficiency.
It has no effect on systems where \fBsplice\fR(2) is unavailable.
.TP
.B \-E, \-\-skip-errors
Ignore read errors by attempting to skip past the offending sections.
The corresponding parts of the output will be null bytes.
At first only a few bytes will be skipped, but if there are many errors in a
row then the skips will move up to chunks of 512.
This is intended to be similar to \*(lq\fIdd\~conv=sync,noerror\fR\*(rq.
.IP
Specify \*(lq\fB\-\-skip\-errors\fR\*(rq twice to only report a read error
once per file, instead of reporting each byte range skipped.
.TP
.BI \-Z\  BYTES \fR,\ \fB\-\-error\-skip\-block\  BYTES
When ignoring read errors with \*(lq\fB\-\-skip\-errors\fR\*(rq, instead of
trying to adaptively skip by reading small amounts and skipping
progressively larger sections until a read succeeds, move to the next file
block of \fIBYTES\fR bytes as soon as an error occurs.
There may still be some shorter skips where the block being skipped
coincides with the end of the transfer buffer.
The same suffixes as \*(lq\fB\-\-size\fR\*(rq can be used.
.IP
This option can only be used with \*(lq\fB\-\-skip\-errors\fR\*(rq
and is intended for use when reading from a block device, such as
\*(lq\fB\-\-skip\-errors\~\-\-error\-skip\-block\~4K\fR\*(rq
to skip in 4 kibibyte blocks.
This will speed up reads from faulty media, at the expense of potentially
losing more data.
.TP
.B \-S, \-\-stop-at-size
If a size was specified with \*(lq\fB\-\-size\fR\*(rq, stop transferring
data once that many bytes have been written, instead of continuing to the
end of input.
.TP
.B \-Y, \-\-sync
After every write operation, synchronise the buffer caches to disk with
\fBfdatasync\fR(2).
This has no effect when the output is a pipe.
Using \*(lq\fB\-\-sync\fR\*(rq may improve the accuracy of the progress bar
when writing to a slow disk.
.TP
.B \-K, \-\-direct-io
Set the \fBO_DIRECT\fR flag on all inputs and outputs, if it is available.
This will minimise the effect of caches, at the cost of performance.
Due to memory alignment requirements, it also may cause read or write
failures with an error of \*(lqInvalid argument\*(rq, especially if reading
and writing files across a variety of filesystems in a single \fBpv\fR call.
Use this option with caution.
.TP
.B \-X, \-\-discard
Instead of transferring input data to standard output, discard it.
This is equivalent to redirecting standard output to \fI/dev/null\fR,
except that \fBwrite\fR(2) is never called.
Implies \*(lq\fB\-\-no\-splice\fR\*(rq.
.TP
.BI \-U\  FILE \fR,\ \fB\-\-store\-and\-forward\  FILE
Instead of passing data through immediately, do it in two stages - first
read all input and write it to \fIFILE\fR, and then once the input is
exhausted, read all of \fIFILE\fR and write it to the output.
\fIFILE\fR remains in place afterwards, unless it is
\*(lq\fB-\fR\*(rq, in which case \fBpv\fR creates a temporary file for this
purpose, and automatically removes it afterwards.
.IP
This can be useful if you have a pipeline which generates data (your
input) quickly but you don't know the size, and you wish to pass it to some
slower process, once all of the input has been generated and you know its
size, so you can see its progress.
Note that when doing this with relatively small amounts of data,
\*(lq\fB\-\-no-splice\fR\*(rq may be preferable so that pipe buffering
doesn't affect the progress display.
.\"
.\"
.SS "Alternative operating modes"
.TP
.BI \-d\  PID\fR[\fB:\fR\fIFD\fR],\  \fB\-\-watchfd\  PID\fR[\fB:\fR\fIFD\fR]
Instead of transferring data, watch file descriptor \fIFD\fR of process
\fIPID\fR, and show its progress.
The \fBpv\fR process will exit when \fIFD\fR either changes to a different
file, changes read/write mode, or is closed; other data transfer modifiers -
and remote control - may not be used with this option.
.IP
If only a \fIPID\fR is specified, then that process will be watched, and all
regular files and block devices it opens will be shown with a progress bar.
The \fBpv\fR process will exit when process \fIPID\fR exits.
.TP
.BI \-R\  PID \fR,\ \fB\-\-remote\  PID
Remotely control another instance of \fBpv\fR with process ID \fIPID\fR,
making it act as though it had been given this instance's command line.
For example, if \*(lq\fBpv\~\-\-rate\-limit\~123K\fR\*(rq is running with
process ID 9876, then running
\*(lq\fBpv\~\-\-remote\~9876\~\-\-rate\-limit\~321K\fR\*(rq will cause
process 9876 to start using a rate limit of 321KiB instead of 123KiB.
Note that some options cannot be changed while running, such as
\*(lq\fB\-\-cursor\fR\*(rq, \*(lq\fB\-\-line\-mode\fR\*(rq,
\*(lq\fB\-\-force\fR\*(rq, \*(lq\fB\-\-delay\-start\fR\*(rq,
\*(lq\fB\-\-skip\-errors\fR\*(rq, and \*(lq\fB\-\-stop\-at\-size\fR\*(rq.
.\"
.SS "Other options"
.TP
.BI \-P\  FILE \fR,\ \fB\-\-pidfile\  FILE
Save the process ID of \fBpv\fR in \fIFILE\fR.
The file will be replaced if it already exists, and will be removed when
\fBpv\fR exits.
While \fBpv\fR is running, \fIFILE\fR will contain a single number - the
process ID of \fBpv\fR - followed by a newline.
.TP
.B \-h, \-\-help
Print a usage message on standard output and exit successfully.
.TP
.B \-V, \-\-version         
Print version information on standard output and exit successfully.
.\"
.SH FORMATTING
Format strings used by \*(lq\fB\-\-format\fR\*(rq and
\*(lq\fB\-\-extra\-display\fR\*(rq can contain the following sequences:
.TP
.BR %p ", " %{progress}
Progress bar (suffixed with a percentage if the size is known).
Equivalent to \*(lq\fB\-\-progress\fR\*(rq.
Expands to fill the remaining space unless prefixed by a number to set the
width, such as \*(lq\fB%20p\fR\*(rq or \*(lq\fB%20{progress}\fR\*(rq.
.TP
.BR %{progress\-bar\-only}
Progress bar, without any sides, and without any percentage displayed
afterwards.
Expands to fill the remaining space unless prefixed by a number.
.TP
.B %{progress\-amount\-only}
The percentage completion (or maximum rate, with \*(lq\fB\-\-gauge\fR\*(rq
when the size is unknown).
.TP
.B %{bar\-plain}
Progress bar in the standard plain format, without any sides, and without
any percentage displayed afterwards.
Expands to fill the remaining space unless prefixed by a number.
.TP
.B %{bar\-block}
Progress bar using Unicode full blocks, without any sides, and without any
percentage displayed afterwards.
Expands to fill the remaining space unless prefixed by a number.
If UTF-8 output is not available, the plain format is used.
.TP
.B %{bar\-granular}
Progress bar using Unicode full blocks, and 1/8th blocks for partial fills,
providing a more granular display.
Like the other \*(lq%{bar}\*(rq strings this shows the bar without any
sides, and without any percentage displayed afterwards, and expands to fill
the remaining space unless prefixed by a number.
If UTF-8 output is not available, the plain format is used.
.TP
.B %{bar\-shaded}
Progress bar using Unicode full blocks and shade characters - dark and
medium shade are used for partial fills, and the light shade is used for the
background.
Like the other \*(lq%{bar}\*(rq strings this shows the bar without any
sides, and without any percentage displayed afterwards, and expands to fill
the remaining space unless prefixed by a number.
If UTF-8 output is not available, the plain format is used.
.TP
.BR %t ", " %{timer}
Elapsed time.
Equivalent to \*(lq\fB\-\-timer\fR\*(rq.
.TP
.BR %e ", " %{eta}
ETA as time remaining.
Equivalent to \*(lq\fB\-\-eta\fR\*(rq.
.TP
.BR %I ", " %{fineta}
ETA as local time at which the transfer will finish.
Equivalent to \*(lq\fB\-\-fineta\fR\*(rq.
.TP
.BR %r ", " %{rate}
Current data transfer rate.
Equivalent to \*(lq\fB\-\-rate\fR\*(rq.
.TP
.BR %a ", " %{average\-rate}
Average data transfer rate.
Equivalent to \*(lq\fB\-\-average\-rate\fR\*(rq.
.TP
.BR %b ", " %{bytes} ", " %{transferred}
Bytes transferred so far (or lines if \*(lq\fB\-\-line\-mode\fR\*(rq was specified).
Equivalent to \*(lq\fB\-\-bytes\fR\*(rq.
If \*(lq\fB\-\-bits\fR\*(rq was specified, \*(lq\fB%b\fR\*(rq shows the bits
transferred so far, not bytes.
.TP
.BR %T ", " %{buffer\-percent}
Percentage of the transfer buffer in use.
Equivalent to \*(lq\fB\-\-buffer\-percent\fR\*(rq.
Displays \*(lq{\-\-\-\-}\*(rq if the transfer is being done with
\fBsplice\fR(2), since splicing to or from pipes does not use the buffer.
.TP
.BR %nA ", " %n{last\-written}
Show the last \fIn\fR bytes written (for example, \*(lq\fB%16A\fR\*(rq shows
the last 16 bytes).
Shows only dots if the transfer is being done with \fBsplice\fR(2), since
splicing to or from pipes does not use the buffer.
.TP
.BR %nL ", " %n{previous\-line}
Show the first \fIn\fR bytes of the most recently written line (for example,
\*(lq\fB%40L\fR\*(rq shows the first 40 bytes).
If no \fIn\fR is given, then this expands to fill the available space.
Shows only spaces if the transfer is being done with \fBsplice\fR(2).
.TP
.BR %N ", " %{name}
Show the name prefix given by \*(lq\fB\-\-name\fR\*(rq.
Padded to 9 characters with spaces, and suffixed with \*(lq:\*(rq.
.TP
.B %{sgr:colour,...}
Emit ECMA-48 SGR (Select Graphic Rendition) codes if the terminal supports
colours, where \fIcolour,...\fR is a comma-separated list of any of the
keywords below, or the numeric values from \fBconsole_codes\fR(4).  If
colour support is not available, nothing is emitted.
.IP
Supported keywords are:
\fBreset\fR or \fBnone\fR,
\fBblack\fR,
\fBred\fR,
\fBgreen\fR,
\fBbrown\fR or \fByellow\fR,
\fBblue\fR,
\fBmagenta\fR,
\fBcyan\fR,
\fBwhite\fR,
\fBfg\-black\fR,
\fBfg\-red\fR,
\fBfg\-green\fR,
\fBfg\-brown\fR or \fBfg\-yellow\fR,
\fBfg\-blue\fR,
\fBfg\-magenta\fR,
\fBfg\-cyan\fR,
\fBfg\-white\fR,
\fBfg\-default\fR,
\fBbg\-black\fR,
\fBbg\-red\fR,
\fBbg\-green\fR,
\fBbg\-brown\fR or \fBbg\-yellow\fR,
\fBbg\-blue\fR,
\fBbg\-magenta\fR,
\fBbg\-cyan\fR,
\fBbg\-white\fR,
\fBbg\-default\fR,
\fBbold\fR,
\fBdim\fR,
\fBitalic\fR,
\fBunderscore\fR or \fBunderline\fR,
\fBblink\fR,
\fBreverse\fR,
\fBno\-bold\fR or \fBno\-dim\fR,
\fBno\-italic\fR,
\fBno\-underscore\fR or \fBno\-underline\fR,
\fBno\-blink\fR,
\fBno\-reverse\fR.
.IP
With colours, the optional "fg-" prefix indicates foreground; a prefix of
"bg-" indicates background.
.IP
For example, \*(lq\fB%{sgr:green,bold}TEXT%{sgr:reset}\fR\*(lq will make
\fITEXT\fR bold green on supported terminals.
.TP
.B %%
A single \*(lq%\*(rq.
.PP
Any other contents are reproduced in the progress display as-is.
.PP
The format string equivalent of the default display switches is
\*(lq\fB%b\~%t\~%r\~%p\~%e\fR\*(rq.
.\"
.SH EXAMPLES
Some suggested common switch combinations:
.TP
.B pv \-ptebar
Show a progress bar, elapsed time, estimated completion time, byte counter,
average rate, and current rate.
.TP
.B pv \-betlap
Show a progress bar, elapsed time, estimated completion time, line counter,
and average rate, counting lines instead of bytes.
.TP
.B pv \-btrpg
Show the amount transferred, elapsed time, current rate, and a gauge showing
the current rate as a percentage of the maximum rate seen - useful in a
pipeline where the total size is unknown.
(If the size \fIis\fR known, these options will show the percentage
completion instead of the rate gauge).
.TP
.B pv \-t
Show only the elapsed time - useful as a simple timer, such as
\*(lq\fBsleep\~10m\~|\~pv\~\-t\fR\*(rq.
.TP
.B pv \-pterb
The default behaviour: progress bar, elapsed time, estimated completion
time, current rate, and byte counter.
.PP
On macOS, it may be useful to specify \*(lq\fB\-\-buffer\-size\~1024\fR\*(rq
in a pipeline, as this may improve performance.
.PP
To watch how quickly a file is transferred using \fBnc\fR(1):
.PP
.in +4
.EX
pv file | nc \-w 1 somewhere.com 3000
.EE
.in
.PP
A similar example, transferring a file from another process and passing the
expected size to \fBpv\fR:
.PP
.in +4
.EX
cat file | pv \-\-size 12345 | nc \-w 1 somewhere.com 3000
.EE
.in
.PP
To watch the progress of creating a tar.gz archive:
.PP
.in +4
.EX
tar cf \- directory/ \e
| pv \-\-size $(du \-sb directory/ | awk '{print $1}') \e
| gzip \-9 \e
> out.tar.gz
.EE
.in
.PP
Taking an image of a disk, skipping errors:
.PP
.in +4
.EX
pv \-EE /dev/your/disk/device > disk-image.img
.EE
.in
.PP
Writing an image back to a disk:
.PP
.in +4
.EX
pv disk-image.img > /dev/your/disk/device
.EE
.in
.PP
Zeroing a disk:
.PP
.in +4
.EX
pv < /dev/zero > /dev/your/disk/device
.EE
.in
.PP
Note that if the input size cannot be calculated, and the output is a block
device, then the size of the block device will be used and \fBpv\fR will
automatically stop at that size as if \*(lq\fB\-\-stop\-at\-size\fR\*(rq had
been given.
.PP
(Linux and macOS only): Watching file descriptor 3 opened by another process 1234:
.PP
.in +4
.EX
pv \-\-watchfd 1234:3
.EE
.in
.PP
(Linux and macOS only): Watching all file descriptors used by process 1234:
.PP
.in +4
.EX
pv \-\-watchfd 1234
.EE
.in
.PP
Rate-limiting the transfer between two processes in a pipeline, with no
display:
.PP
.in +4
.EX
producer | pv \-\-quiet \-\-rate\-limit 1M | consumer
.EE
.in
.PP
Sending logs to a processing script, showing the most recent line as part of
the progress display:
.PP
.in +4
.EX
pv \-\-format '%a %p : %L' big.log | processing-script
.EE
.in
.PP
Showing progress as lines of JSON data:
.PP
.in +4
.EX
pv \-\-numeric \-\-format '{"elapsed":%t,"bytes":%b,"rate":%r,"percentage":%{progress-amount-only}}' big.log | processing-script
.EE
.in
.\"
.SH EXIT STATUS
An exit status of 1 indicates a problem with the \*(lq\fB\-\-remote\fR\*(rq
or \*(lq\fB\-\-pidfile\fR\*(rq options.
.PP
Any other exit status is a bitmask of the following:
.TP 5
\~\fB2\fR
One or more files could not be accessed, \fBstat\fR(2)ed, or opened.
.TP
\~\fB4\fR
An input file was the same as the output file.
.TP
\~\fB8\fR
Internal error with closing a file or moving to the next file.
.TP
\~\fB16\fR
There was an error while transferring data from one or more input files.
.TP
\~\fB32\fR
A signal was caught that caused an early exit.
.TP
\~\fB64\fR
Memory allocation failed.
.PP
A zero exit status indicates no problems.
.\"
.SH ENVIRONMENT
The following environment variables may affect \fBpv\fR:
.TP
.B HOME
The current user's home directory.
This may be used by \*(lq\fB\-\-remote\fR\*(rq to exchange messages between
\fBpv\fR instances: if the \fI/run/user/UID/\fR directory does not exist
(where \fIUID\fR is the current user ID), then \fI$HOME/.pv/\fR will be used
instead.
.TP
.BR TMPDIR ", " TMP
The directory to create per-tty lock files for the terminal when using
\*(lq\fB\-\-cursor\fR\*(rq.
If \fBTMPDIR\fR is set to a non-empty value, it is the directory under which
lock files are created.
Otherwise, \fBTMP\fR is used.
If neither are set, then \fI/tmp\fR is used.
.\"
.SH NOTES
In some versions of \fBbash\fR(1) and \fBzsh\fR(1), the construct
\*(lq\fB<(pv\~filename)\fR\*(rq will not output any progress to the terminal
when run from an interactive shell, due to the subprocess being run in a
separate process group from the one that owns the terminal.
In these cases, use \*(lq\fB\-\-force\fR\*(rq.
.PP
If \fBpv\fR is used in a pipeline in \fBzsh\fR version 5.8, and the last
command in the pipeline is based on shell builtins, \fBzsh\fR takes control
of the terminal away from \fBpv\fR, preventing progress from being
displayed.
For example, this will produce no progress bar:
.PP
.in +4n
.EX
pv InputFile | { while read \-r line; do sleep 0.1; done; }
.EE
.in
.PP
To work around this, put the last commands of the pipeline in
normal brackets to force the use of a subshell:
.PP
.in +4n
.EX
pv InputFile | ( while read \-r line; do sleep 0.1; done; )
.EE
.in
.PP
Refer to
.UR https://codeberg.org/ivarch/pv/issues/105
issue #105
.UE
for full details.
.PP
The \*(lq\fB\-\-remote\fR\*(rq option requires that either
\fI/run/user/<uid>/\fR or \fI$HOME/\fR can be written to, for inter-process
communication.
.PP
The \*(lq\fB\-\-size\fR\*(rq option has no effect if used with
\*(lq\fB\-\-watchfd\fR\~\fIPID\fR\*(rq to watch all file descriptors of a
process, but will work with \*(lq\fB\-\-watchfd\fR\~\fIPID\fR:\fIFD\fR\*(rq
to watch a single file descriptor.
.PP
If the input size cannot be calculated, and the output is a block device,
then \fBpv\fR will read the output device's size, use that as if it had been
passed to \*(lq\fB\-\-size\fR\*(rq, and activate
\*(lq\fB\-\-stop\-at\-size\fR\*(rq.
.PP
The \*(lq\fB%nA\fR\*(rq and \*(lq\fB%nL\fR\*(rq format sequences may not be
effective with small input files, and \*(lq\fB%nL\fR\*(rq may be a few lines
out due to buffering within the pipeline itself.
.PP
Numbers passed to \*(lq\fB\-\-size\fR\*(rq, \*(lq\fB\-\-rate\-limit\fR\*(rq,
\*(lq\fB\-\-buffer\-size\fR\*(rq, and \*(lq\fB\-\-error\-skip\-block\fR\*(rq
may all be expressed as decimals if followed by a suffix, so for example
\*(lq\fI\-\-size\~1.5G\fR\*(rq is equivalent to
\*(lq\fI\-\-size\~1536M\fR\*(rq.
.PP
Numbers passed to \*(lq\fB\-\-interval\fR\*(rq and
\*(lq\fB\-\-delay\-start\fR\*(rq may be integers or decimals, but may not
have a suffix.
.PP
Numbers passed to \*(lq\fB\-\-last\-written\fR\*(rq,
\*(lq\fB\-\-width\fR\*(rq, \*(lq\fB\-\-height\fR\*(rq,
\*(lq\fB\-\-average\-rate\-window\fR\*(rq, and \*(lq\fB\-\-remote\fR\*(rq
must be integers with no suffix.
.\"
.SH REPORTING BUGS
Please report bugs or feature requests via the issue tracker linked from the
.UR https://ivarch.com/programs/pv.shtml
\fBpv\fR home page
.UE .
.\"
.SH "SEE ALSO"
.BR cat (1),
.BR splice (2),
.BR fdatasync (2),
.BR open (2)
(for \fBO_DIRECT\fR),
.BR console_codes (4)
.\"
.SH COPYRIGHT
Copyright \(co 2002-2008, 2010, 2012-2015, 2017, 2021, 2023-2025 Andrew Wood.
.PP
License GPLv3+:
.UR https://www.gnu.org/licenses/gpl-3.0.html
GNU GPL version 3 or later
.UE .
.PP
This is free software: you are free to change and redistribute it.  There is
NO WARRANTY, to the extent permitted by law.
.PP
Please see the package's ACKNOWLEDGEMENTS file for a complete list of
contributors.
